


import 'dart:math';

import 'package:flutter/material.dart';
import 'package:slivers/components/drawer_samples.dart';

class AnimationSample1 extends StatefulWidget {
  const AnimationSample1({Key? key}) : super(key: key);

  @override
  _AnimationSample1State createState() => _AnimationSample1State();
}

class _AnimationSample1State extends State<AnimationSample1> {

  double _width = 100;
  double _height = 100;
  Color _color = Colors.red;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(title:Text("Animations 2")),
      drawer: DrawerSamples(),

      floatingActionButton: FloatingActionButton(
          onPressed: (){
              var random = Random();
              setState((){
                _width = random.nextInt(300).toDouble();
                _height = random.nextInt(300).toDouble();
                _color = Color.fromRGBO(
                    random.nextInt(255),
                    random.nextInt(255),
                    random.nextInt(255),
                    1);
              });

          },
          child:Icon(Icons.play_arrow)
      ),
      body: Center(
        child: AnimatedContainer(
          width: _width,
          height: _height,
          color: _color,
          duration: Duration(seconds:1),

        ),
      ),
    );
  }
}
