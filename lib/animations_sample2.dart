


import 'dart:math';

import 'package:flutter/material.dart';
import 'package:slivers/components/drawer_samples.dart';

class AnimationSample2 extends StatefulWidget {
  const AnimationSample2({Key? key}) : super(key: key);

  @override
  _AnimationSample2State createState() => _AnimationSample2State();
}

class _AnimationSample2State extends State<AnimationSample2>
    with TickerProviderStateMixin{

  double _value = 0;
  double _opacity = 0;

  AnimationController? _animationController;

  @override
  initState(){
    initAnimationController();
  }

  initAnimationController(){
    _animationController = AnimationController(
      duration:Duration(seconds:3), vsync: this,
    );

    _animationController!.repeat();

    setState(() {

    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(title:Text("Animations 3")),
      drawer: DrawerSamples(),

      floatingActionButton: FloatingActionButton(
          onPressed: (){
              setState((){
                _opacity == 0 ? _opacity = 1 : _opacity = 0;
              });

          },
          child:Icon(Icons.play_arrow)
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children:[

            FadeInImage.assetNetwork(
                width: 300,
                height: 400,
                fit: BoxFit.contain,
                placeholder:"assets/image.png",
                image: "https://th.bing.com/th/id/R.b3922d68122d1d7527c1e7a6b24880f1?rik=BwZ%2bGb4azL%2f1bQ&riu=http%3a%2f%2ftktimelapse.com%2fsamples%2fbig%2fNAT-0015-HD.jpg&ehk=C%2fjxbbmZKC5LdNQ6HT%2foup%2bY6Idp3MPwMK3yKcw8Hj4%3d&risl=&pid=ImgRaw&r=0"
            ),

            if(_animationController != null)
            RotationTransition(
                turns:_animationController!,
                child: ScaleTransition(
                    scale:_animationController!,
                    child: Container(
                        width:100,
                        height:100,
                        color:Colors.green
                    )
                )
            ),



            AnimatedOpacity(
              duration: Duration(milliseconds:600),
              opacity: _opacity,
              child: Container(
                width:100,
                height:100,
                color:Colors.green
              )
            ),

            TweenAnimationBuilder<double>(
                tween: Tween(begin:0.0, end: _value),
                duration: Duration(milliseconds: 600),
                child: Container(width:50,height:50,color:Colors.red),
                builder: (context,value,child){
                  return Transform.translate(
                    offset: Offset(value * 200 - 100,0),
                    child: child
                  );
                }
            )

           ,


            Slider.adaptive(
                value: _value, onChanged: (value)=>{
                setState((){
                  print("VALUE IS: ${value}");
                  _value = value;
                })
            })
          ]


        ),
      ),
    );
  }
}
