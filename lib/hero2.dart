import 'dart:developer';

import 'package:flutter/material.dart';

class HeroPage2 extends StatelessWidget {
  String image_url;

  HeroPage2({Key? key,required this.image_url}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title:Text("Hero 2")),

        body: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Container(
              width:MediaQuery.of(context).size.width,
              height:500,
              child:Hero(
                tag:image_url!,
                child: Image.network("https://image.freepik.com/free-vector/happy-new-year-2020-background_42237-539.jpg",fit: BoxFit.fitWidth,))

              ),
          ],
        )
        );

  }
}
