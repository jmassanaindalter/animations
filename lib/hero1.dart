import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:slivers/animation_wiget.dart';
import 'package:slivers/animations_sample1.dart';
import 'package:slivers/animations_sample2.dart';
import 'package:slivers/components/drawer_samples.dart';
import 'package:slivers/hero2.dart';
import 'package:slivers/slivers_demo.dart';

class HeroPage1 extends StatelessWidget {
  HeroPage1({Key? key}) : super(key: key);

  var images = [
    "https://cdn.vox-cdn.com/thumbor/fi3cJdAMgez-NqbPZWxK1kdBOEM=/0x0:2040x1360/1200x800/filters:focal(857x517:1183x843)/cdn.vox-cdn.com/uploads/chorus_image/image/67718858/acastro_191014_1777_google_pixel_0005.0.0.jpg",
    "https://image.freepik.com/free-vector/happy-new-year-2020-background_42237-539.jpg",
    "https://supertipp-online.de/wp-content/uploads/2021/05/Bild_KulturTandem_28.5.-Livestream-768x575.jpg"
  ];

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop:() async{
        final value = await showDialog<bool>(
          context: context,
          builder: (context){
            return AlertDialog(
              content: Text("¿Seguro que quieres salir?"),
              actions:[
                ElevatedButton(
                  onPressed: (){
                    Navigator.of(context).pop(false);
                  },
                  child:Text("NO")
                ),
                ElevatedButton(
                    onPressed: (){
                      Navigator.of(context).pop(true);
                    },
                    child:Text("YES")
                )
              ]
            );
          }
        );

        return value == true;

      },
      child: Scaffold(
        appBar: AppBar(title:Text("Hero Sample")),
        drawer: DrawerSamples(),
        body: ListView.builder(
          itemCount:3,
          itemBuilder: (_,index){
            return Padding(
              padding: const EdgeInsets.only(top:8.0),
              child: GestureDetector(
                onTap: (){
                  Navigator.of(context).push(
                    MaterialPageRoute(builder:(_)=>HeroPage2(image_url:images[index]))
                  );
                },
                child: ListTile(
                  leading: Container(
                      width:200,
                      child: Hero(
                          tag:images[index],
                          child: Image.network(images[index],fit: BoxFit.fitWidth,))),
                  title: Text("Imagen ${index}")
                ),
              ),
            );
          }


        ),
      ),
    );
  }
}

