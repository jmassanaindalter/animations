import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:slivers/components/drawer_samples.dart';

class AnimationWidget extends StatefulWidget {
  AnimationWidget({Key? key}) : super(key: key);

  @override
  State<AnimationWidget> createState() => _AnimationWidgetState();
}

class _AnimationWidgetState extends State<AnimationWidget>
    with SingleTickerProviderStateMixin{

  Animation<double>? animation;
  AnimationController? controller;

  bool _growed = false;

  @override
  initState(){
    super.initState();

    controller =
        AnimationController(duration: const Duration(seconds:1),vsync:this);

    animation = Tween<double>(begin: 0, end: 300).animate(controller!);

    animation!.addListener(() {
      setState(() {

      });
    });

    animation!.addStatusListener((status) {
      if(status == AnimationStatus.completed) {
        controller?.reverse();
        log("Reached final");
      }else if(status == AnimationStatus.dismissed){
        controller?.forward();
        log("Reached beginning");
      }

    });

    controller!.forward();
    _growed = true;




  }

  @override
  dispose(){
    controller?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(title:Text("Animation 1")),
      drawer: DrawerSamples(),

      floatingActionButton: FloatingActionButton(
          onPressed: (){
            _growed = !_growed;
            if(_growed) {
              controller!.reverse();
            }else{
              controller!.forward();
            }
          },
          child:Icon(Icons.play_arrow)
      ),
      body: Center(
          child: Container(
              height:animation?.value,
              width:animation?.value,
              child: FlutterLogo()
          )
      ),
    );
  }
}