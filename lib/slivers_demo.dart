import 'package:flutter/material.dart';

import 'sliver_header.dart';



class SliversDemo extends StatefulWidget {
  @override
  _SliversDemoState createState() => _SliversDemoState();
}

class _SliversDemoState extends State<SliversDemo> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final List<String> _name = ['tester'];

  Widget _buildName() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Name'),

    );
  }

  Widget _buildFixedList(Color color, String _text) {
    return Container(
      color: color,
      child: Center(
        child: Text(
          _text,
          style: TextStyle(color: Colors.white, fontSize: 25),
        ),
      ),
    );
  }

  void _submit() {
    if (!_formKey.currentState!.validate()) {
      return;
    }

    _formKey.currentState!.save();
  }

  @override
  Widget build(BuildContext context) {
    print(_name.toString());

    return SafeArea(
      child: Scaffold(
        appBar:AppBar(title:Text("Slivers Demo")),

        backgroundColor: Color(0xFFEDF2F8),
        body: CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              title: Text('Flutter Slivers Demo',style:
              TextStyle(fontSize: 25,
                  fontWeight: FontWeight.bold,
                  color: Colors.white),),
              backgroundColor: Color(0xFFEDF2F8),
              expandedHeight: 200,
              flexibleSpace: FlexibleSpaceBar(
                background: Image.network(
                    "https://images.unsplash.com/photo-1603785608232-44c43cdc0d70?ixlib=rb-1.2.1&"
                        "ixid=MXwxMjA3fDB8MHx0b3BpYy1mZWVkfDY4fEo5eXJQYUhYUlFZfHxlbnwwfHx8&auto=format&"
                        "fit=crop&w=500&q=60",
                    fit: BoxFit.cover),
              ),
            ),
            SliverHeader(Colors.red, "SliverPersistentHeader 1"),
            SliverHeader(Colors.blue, "SliverPersistentHeader 2"),
            SliverHeader(Colors.purple, "SliverPersistentHeader 3"),
            SliverFixedExtentList(
              itemExtent: 70,
              delegate: SliverChildListDelegate([
                _buildFixedList(Colors.cyan, "Cyan"),
                _buildFixedList(Colors.green, "Green"),
                _buildFixedList(Colors.orange, "Orange"),
                _buildFixedList(Colors.amberAccent, "AmberAccent"),
                _buildFixedList(Colors.blueGrey, "Blue Grey"),
                _buildFixedList(Colors.blueGrey, "Blue Grey"),
                _buildFixedList(Colors.cyan, "Cyan"),
                _buildFixedList(Colors.green, "Green"),
                _buildFixedList(Colors.orange, "Orange"),
                _buildFixedList(Colors.amberAccent, "AmberAccent"),
                _buildFixedList(Colors.blueGrey, "Blue Grey"),
                _buildFixedList(Colors.blueGrey, "Blue Grey"),
                _buildFixedList(Colors.blueGrey, "Blue Grey"),
                _buildFixedList(Colors.cyan, "Cyan"),
                _buildFixedList(Colors.green, "Green"),
                _buildFixedList(Colors.orange, "Orange"),
                _buildFixedList(Colors.amberAccent, "AmberAccent"),
                _buildFixedList(Colors.blueGrey, "Blue Grey"),
                _buildFixedList(Colors.blueGrey, "Blue Grey"),
                _buildFixedList(Colors.blueGrey, "Blue Grey"),
                _buildFixedList(Colors.cyan, "Cyan"),
                _buildFixedList(Colors.green, "Green"),
                _buildFixedList(Colors.orange, "Orange"),
                _buildFixedList(Colors.amberAccent, "AmberAccent"),
                _buildFixedList(Colors.blueGrey, "Blue Grey"),
                _buildFixedList(Colors.blueGrey, "Blue Grey"),

              ]),
            ),


          ],
        ),
      ),
    );
  }
}

Color _randomColor(int index) {
  if (index % 4 == 0) {
    return Colors.blue;
  } else if (index % 4 == 1) {
    return Colors.orange;
  }else if(index % 4 == 2){
    return Colors.cyan;
  }
  return Colors.red;
}