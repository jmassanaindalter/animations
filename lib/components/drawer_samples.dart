import 'package:flutter/material.dart';
import 'package:slivers/animation_wiget.dart';
import 'package:slivers/animations_sample1.dart';
import 'package:slivers/animations_sample2.dart';
import 'package:slivers/hero1.dart';
import 'package:slivers/slivers_demo.dart';

class DrawerSamples extends StatelessWidget {
  const DrawerSamples({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child:ListView(
          children: ListTile.divideTiles(
              context:context,
              tiles:[
                ListTile(
                    onTap: (){
                      Navigator.of(context).push(
                          MaterialPageRoute(
                              builder:(_)=> HeroPage1()
                          )
                      );
                    },
                    title:Text("Hero Sample 1")
                ),
                ListTile(
                    onTap: (){
                      Navigator.of(context).push(
                          MaterialPageRoute(
                              builder:(_)=> AnimationWidget()
                          )
                      );
                    },
                    title:Text("Animations 1")
                ),
                ListTile(
                    onTap: (){
                      Navigator.of(context).push(
                          MaterialPageRoute(
                              builder:(_)=> AnimationSample1()
                          )
                      );
                    },
                    title:Text("Animations 2")
                ),
                ListTile(
                    onTap: (){
                      Navigator.of(context).push(
                          MaterialPageRoute(
                              builder:(_)=> AnimationSample2()
                          )
                      );
                    },
                    title:Text("Animations 3")
                ),
                ListTile(
                    onTap: (){
                      Navigator.of(context).push(
                          MaterialPageRoute(
                              builder:(_)=> SliversDemo()
                          )
                      );
                    },
                    title:Text("Slivers Sample")
                )
              ]
          ).toList()
      ),
    );
  }
}
